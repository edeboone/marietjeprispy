#!/usr/bin/env python3

from mpris_server.server import Server


from marietjelibpy import Marietje as Marietje

import logging

# Local
from adapter import MarietjeMprisAdapter
from eventhandler import MarietjeMprisEventHandler
from utils import RepeatedTimer

def create_adapters_and_server(config):
    marietje = Marietje( config )
    # Make sure we have a session
    if not marietje.login():
        raise Exception("Cannot log into Marietje..");


    # Initialise with MarietjeAdapter
    marietjeMprisAdapter = MarietjeMprisAdapter(marietje)
    mpris = Server('MarietjeMPRIS', adapter=marietjeMprisAdapter)

    return mpris

def main(config):
    logging.basicConfig(level=logging.INFO)
    print("Creating Adapter and Server")
    mpris = create_adapters_and_server(config)

    if not mpris:
        raise Exception("Cannot start MPRIS")

    print("Setting up Event Handler")
    # Setup Event Handlers
    event_handler = MarietjeMprisEventHandler(root=mpris.root, player=mpris.player)

    N = 5 # seconds
    rt = RepeatedTimer(N, event_handler.on_title)

    print("Publishing and running")
    try :
        # Publish and run
        mpris.publish()
        mpris.loop()
    finally:
        print("Stopping")
        rt.stop()

if __name__ == "__main__":
    from user_config import config as user_config

    main(user_config)
