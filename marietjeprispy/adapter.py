from mpris_server.base import Artist, Track
from mpris_server.adapters import MprisAdapter, PlayState
import mpris_server.adapters as adapters

from marietjelibpy import Marietje

class MarietjeMprisAdapter(MprisAdapter):
    def __init__(self, marietje: Marietje):
        self.marietje = marietje
        super().__init__(marietje.NAME + "NAME")

    def get_uri_schemes(self):
        return []

    def get_mime_types(self):
        return []

    def can_control(self):
        return False

    def get_playstate(self):
        return PlayState.PLAYING

    def is_repeating(self):
        return False

    def is_playlist(self):
        return False

    def get_current_position(self):
        data = self.marietje.get_general_info()
        cur_time = data['current_time']
        start_time = data['started_at']

        return cur_time - start_time

    def get_desktop_entry(self):
        # TODO: make this point to the .desktop file
        return ""

    def get_current_track(self):
        data = self.marietje.get_current_song()

        track_id = data['song']['id']
        duration = data['song']['duration'] * 1000000
        artist1 = Artist(data['song']['artist'])

        track = Track(
            name=self.get_stream_title(),
            artists=(artist1,),
            length=int(duration),
        )
        return track

    def get_stream_title(self):
        title = self.marietje.get_current_song()['song']['title']
        return title

    # Not implemented functions
    def can_quit(self):
        return False
    def quit(self):
        pass
    def next(self):
        pass
    def previous(self):
        pass
    def resume(self):
        pass
    def stop(self):
        pass
    def play(self):
        pass
    def seek(self, time):
        pass
    def open_uri(self, uri):
        pass
    def set_repeating(self, val):
        return False
    def set_loop_status(self, val):
        return False
    def set_rate(self, val):
        return False
    def get_shuffle(self):
        return False
    def set_shuffle(self, val):
        return False
    def get_art_url(self, track):
        return False
    def get_volume(self):
        return False
    def set_volume(self, val):
        return False
    def is_mute(self):
        return False
    def set_mute(self, val):
        return False
    def can_go_next(self):
        return False
    def can_go_previous(self):
        return False
    def can_play(self):
        return False
    def can_pause(self):
        return False
    def can_seek(self):
        return False
    def get_previous_track(self):
        return False
    def get_next_track(self):
        return False
    def metadata(self):
        pass
