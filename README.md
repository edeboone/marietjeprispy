# Marietje MPRIS

This is a little script that registers a MPRIS Player to show what Marietje is currently playing.

It uses the [Python Marietje Library](https://gitlab.science.ru.nl/edeboone/marietjelibpython) to interface with Marietje.

## Installation
Install the following Python packages (taken from [./requirements.txt](./requirements.txt)):
 * `PyGObject` (on Arch this is in `python-gobject`)
 * `pydbus` (on Arch this is in `python-pydbus`)

We also need [`mpris_server`](https://github.com/alexdelorenzo/mpris_server) which can be installed using PyPI:
 * `pip3 install mpris_server`

and finally from the [Science GitLab](https://gitlab.science.ru.nl) we need to install 
 * [`marietjelibpy`](https://gitlab.science.ru.nl/edeboone/marietjelibpython)


## Usage
You can make your own configuration from [./marietjeprispy/user_config.py.sample](./marietjeprispy/user_config.py.sample).
Afterwards, you should be able to 'Just Run' `marietjeprispy.py`.

Testing the configuration can be done with `marietjecli.py` in [`marietjelibpy`].
